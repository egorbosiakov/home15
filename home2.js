let arr = [
  { name: "igor", age: 15 },
  { name: "lena", age: 52 },
];

///////-------1.2
function ageSort(a, b) {
  if (a.age < b.age) return -1;
  if (a.age === b.age) return 0;
  if (a.age > b.age) return 1;
}
console.log(arr.sort(ageSort));

//////////////-------2.2
function search(array, name) {
  return array.find(function (item) {
    return item.name === name;
  });
}
console.log(search(arr, "lena"));
/////////--------------3.2
function wet(array, name) {
  return array.filter(function (item) {
    return item.name === name;
  });
}
console.log(search(arr, "lena"));
//////////------------4.2

arr.forEach((element) => console.log(element));
/////////////------5.2

let newArr = arr.map(function (item) {
  return { ...item, age: item.age >= 50 ? item.age : item.age + 1 };
});
console.log(newArr);
//////////////------6.2
let array = [13, 5, "eee"];

///--не понятно
